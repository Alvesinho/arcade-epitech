/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** Parse
*/

#include "../include/Parse.hpp"

Arcade::Parse::Parse()
{
}


std::vector<std::string> Arcade::Parse::readMap()
{
    std::string map = "map.txt";
    std::ifstream my_file(map);


    if (my_file) {
        while (getline(my_file, map)) {
            Arcade::Parse::Map.push_back(map);
      }
    }
    else {
        throw std::string("Impossible to open");
    }
    return (Map);
}

std::vector<std::string> Arcade::Parse::readMapPacman()
{
    std::string map = "map_pacman.txt";
    std::ifstream my_file(map);


    if (my_file) {
        while (getline(my_file, map)) {
            Arcade::Parse::MapPacman.push_back(map);
      }
    }
    else {
        throw std::string("Impossible to open");
    }
    return (MapPacman);
}



Arcade::Parse::~Parse()
{
}
