/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** parser
*/

#include "../include/core.hpp"
#include "../include/SDL.hpp"
#include "../include/include.hpp"
#include "../include/ncurses_menu.hpp"
#include "../include/Parse.hpp"
#include "../include/ncurses_snake.hpp"
#include "../include/SDL_Map.hpp"
#include "../include/GameSnake.hpp"
#include "../include/parser.hpp"
#include <string.h>
#include <dlfcn.h>
#include <fstream>

parser::parser()
{
}

parser::~parser()
{
}

int parser::errors_gestions(int ac)
{
    if (ac != 2) {
        std::cerr << "The program need 2 arguments for start" << std::endl;
        return (84);
    }
    return (0);
}

int parser::read_file(int ac, char **av)
{
    int ret = 0;
    menu_ncurses menu;
    core c;

    if (ac == 2) {
        if (strcmp("lib_graph/lib_arcade_sfml.so", av[1]) == 0) {
            ret = 1;
            this->lib = 1;
            this->game = 0;
            return (ret);
        }
        if (strcmp("lib_graph/lib_arcade_sdl.so", av[1]) == 0) {
            ret = 3;
            this->lib = 3;
            this->game = 0;
            return (ret);
        }
        if (strcmp("lib_graph/lib_arcade_ncurses.so", av[1]) == 0) {
            ret = 2;
            this->lib = 2;
            this->game = 0;
            return (ret);
        }
    }
    return (ret);
}

std::string parser::recup_value(std::string str)
{
    int pos = str.find_last_of(' ');

    return (str.substr(pos));
}

void *load_lib(std::string path)
{
	void *handle = NULL;

	handle = dlopen(path.c_str(), RTLD_LAZY);
	if (handle == NULL) {
            std::cout << "error :" << dlerror() << std::endl;
            return (NULL);
        }
    dlerror();
	return (handle);
}

void *close_lib(void *handle)
{
   dlclose(handle);
}

void *sym_lib(void *handle, std::string lib)
{
    return (dlsym(handle, lib.c_str()));
}

int main(int ac, char **av)
{

    parser parser;
    core c;
    void *ptr = NULL;
    int ret = 0;
    int lib = 0;

    if (ac == 1) {
        std::cerr << "You need to put a lib .so\n";
        exit(84);
    }
    parser.errors_gestions(ac);
    void *libhandle = load_lib(av[1]);
    ptr = sym_lib(libhandle, "graph");
    ptr = sym_lib(libhandle, "make_snake_ncurses");
    lib = parser.read_file(ac, av);
    c.chooseGame(lib);
    close_lib(libhandle);
    return (ret);
}
