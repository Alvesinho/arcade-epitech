/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** core
*/

#include "../include/pacman.hpp"
#include "../include/core.hpp"
#include "../include/SDL.hpp"
#include "../include/include.hpp"
#include "../include/ncurses_menu.hpp"
#include "../include/Parse.hpp"
#include "../include/ncurses_snake.hpp"
#include "../include/SDL_Map.hpp"
#include "../include/GameSnake.hpp"
#include "../include/mapping.hpp"
#include "../include/map.hpp"
#include "../include/ncurses_pacman.hpp"
#include "../include/SDL_MenuSnak.hpp"

void core::chooseGame(int lib)
{
    this->checkLib = lib;
    try
    {
         while (this->checkGame == 0) {
            Arcade::Parse P;
            this->Map = P.readMap();
            this->MapPacman = P.readMapPacman();
            if (this->checkLib == 1)
                go_to_sfml();
            if (this->checkLib == 3)
                go_to_sdl();
            if (this->checkLib == 2)
                go_to_ncurses();
            if (this->checkLib == 0)
                exit(EXIT_FAILURE);
        }
        game::GameSnake Gs;
        snake_ncurses ncurses_snake;
        Pac::pacman pac;
        SDL_map::SDL_Map map;
        SDL_MenuSnak snake;
        arcadeGame::SFMLGame S;
        while (this->checkGame == 1) {
            
            this->Map = Gs.beginSnake(this->Map, this->touche);
           if (this->checkLib == 3) {
                this->touche = map.SDL_drawMap(this->Map);
                if (this->touche == 5) {
                    SDL_Quit();
                    this->checkGame = 1;
                    this->checkLib = 1;
                }
                if (this->touche == 6) {
                    SDL_Quit();
                    this->checkGame = 1;
                    this->checkLib = 2;
                }
                if (this->touche == 9) {
                     this->checkGame = 2;
                     break;
                }
            }
            if (this->checkLib == 2) {
                this->touche = ncurses_snake.game_loop(this->Map);
                if (ncurses_snake.Exit == 1) {
                     this->game_over_snake(0);
                     return;
                }
                else if (ncurses_snake.Exit == 2) {
                     this->game_win_snake();
                     return;
                }
                if (this->touche == 5) {
                    this->checkGame = 1;
                    this->checkLib = 3;
                }
                if (this->touche == 6) {
                    this->checkGame = 1;
                    this->checkLib = 1;
                }
                if (this->touche == 9) {
                    this->checkGame = 2;
                    this->checkLib = 2;
                }
            }
                if (this->checkLib == 1) {
                this->Map = Gs.beginSnake(this->Map, this->touche);
                this->touche = S.IsOpenGame(this->Map);
                if (this->touche == 55) {
                    S.~SFMLGame();
                    this->checkGame = 1;
                    this->checkLib = 2;
                    S.IsClose();
                }
                if (this->touche == 66) {
                    S.~SFMLGame();
                    this->checkGame = 1;
                    this->checkLib = 3;
                    S.IsClose();
                }

                if (this->touche == 77) {
                    this->checkGame = 2;
                    this->checkLib = 1;
                    arcadeGame::SFMLGame S;
                    S.IsClose();
                    break;
                }
            }
            if (this->checkLib == 0)
                exit(EXIT_FAILURE);
            if (this->checkGame == 2)
                break;
        }
        while (this->checkGame == 2) {
            pacman_ncurses ncurses_pacman;
            if (this->checkLib == 3) {
                SDL_map::SDL_Map map;
                this->MapPacman = pac.beginPacman(this->MapPacman, this->touche);
                this->touche = map.SDL_drawMap(this->MapPacman);
                 if (this->touche == 10) {
                     this->checkGame = 1;
                     chooseGame(3);
                     break;
                }
                if (this->touche == 6) {
                    SDL_Quit();
                    this->checkGame = 1;
                    this->checkLib = 2;
                }
                }
            if (this->checkLib == 2) {
                this->MapPacman = pac.beginPacman(this->MapPacman, this->touche);
                this->touche = ncurses_pacman.game_loop_pacman(this->MapPacman);
                if (this->touche == 5) {
                    this->checkGame = 2;
                    this->checkLib = 3;
                }
                if (this->touche == 6) {
                    this->checkGame = 2;
                    this->checkLib = 1;
                }
                if (this->touche == 10) {
                    this->checkGame = 1;
                    this->checkLib = 2;
                }
            }

                if (this->checkLib == 1) {
                this->MapPacman = pac.beginPacman(this->MapPacman, this->touche);
                this->touche = S.IsOpenGame(this->MapPacman);
                
                if (this->touche == 55) {
                    S.~SFMLGame();
                    this->checkGame = 2;
                    this->checkLib = 2;
                    S.IsClose();
                }
                if (this->touche == 66) {
                    S.~SFMLGame();
                    this->checkGame = 2;
                    this->checkLib = 3;
                    S.IsClose();
                }

                if (this->touche == 77) {
                    this->checkGame = 1;
                    this->checkLib = 1;
                    S.IsClose();
                    break;
                }
                }

        }
    }
    catch(const std::string& e) {
        std::cerr << e << std::endl;
    }
}

void core::game_win_snake()
{
    snake_ncurses ncurses_snake;
    SDL_map::SDL_Map map;
    if (this->checkLib == 2) {
        ncurses_snake.ncurses_game_win();
        this->checkGame = 0;
    }
    else if (this->checkLib == 3) {
        map.SDL_Win();
    }
   
}

void core::game_over_snake(int nb)
{
    snake_ncurses ncurses_snake;
    SDL_map::SDL_Map map;
    

    if (this->checkLib == 2) {
        ncurses_snake.ncurses_game_over();
        this->checkGame = 0;
    }
    else if (this->checkLib == 3) {
            std::cout << "dommage! votre score est de " << Score<int>(nb) << std::endl;
    map.SDL_gameOver();
    }
}

void core::game_over_Pacman(double nb)
{
    snake_ncurses ncurses_snake;
    SDL_map::SDL_Map map;

    if (this->checkLib == 2) {
        ncurses_snake.ncurses_game_over();
        this->checkGame = 0;
    }
    else if (this->checkLib == 3) {
            std::cout << "dommage! votre score est de " << Score<double>(nb) << std::endl;
    map.SDL_gameOver();
    }

}

int core::go_to_ncurses()
{
    int ret = 0;
    menu_ncurses menu;
    ret = menu.ncurses_menu();
    this->checkGame = menu.game;
    this->checkLib = menu.lib;
    while(this->checkLib == 2)
        chooseGame(this->checkLib);
    return (ret);
}

int core::go_to_sfml()
{
    arcade::SFMLGraphic s;
    this->checkLib = s.commetuveux();
    s.IsClose();
    if (this->checkLib == 2 || this->checkLib == 3)
        chooseGame(this->checkLib);
    if (this->checkLib == 100) {
        this->checkLib = 1;
        this->checkGame = 1;
    }
    if (this->checkLib == 200) {
        this->checkLib = 1;
        this->checkGame = 2;
    }

    return (0);
}

int core::go_to_sdl()
{
     if (SDL_Init(SDL_INIT_VIDEO) == -1)
        throw std::string("Erreur d'initialisation de la SDL");
    SDL s;
    s.init_background();
    s.init_position();
    s.init_image();
    s.blit_to_surface();
    s.flip_ecran();
    this->checkLib = s.pause();
    if (this->checkLib == 4) {
        this->checkLib = 3;
        this->checkGame = 1;
    }
    else if (this->checkLib == 8) {
        this->checkLib = 3;
        this->checkGame = 2;
    }
    s.freeSpace();
    SDL_Quit();
    chooseGame(this->checkLib);
    return (0);
}

core::core(int c)
{
    this->checkGame=0;
    this->checkLib=c;
}
core::core()
{
    this->checkGame = 0;
    this->checkLib = 3;
}

core::~core(void)
{
}
