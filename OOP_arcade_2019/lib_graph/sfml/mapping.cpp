/*
** EPITECH PROJECT, 2020
** Mapping
** File description:
** Inho
*/

#include "../../include/mapping.hpp"
#include "../../include/core.hpp"
#include "../../include/GameSnake.hpp"
#include "../../include/pacman.hpp"

int arcadeGame::SFMLGame::JeuContinue(std::vector<std::string> Map)
{
    IsOpenGame(Map);
    std::cout << touchegame << std::endl;
    return (touchegame);
}
int arcadeGame::SFMLGame::GameSnake(std::vector<std::string> Map)
{  
    window.setFramerateLimit(80);
    IsOpenGame(Map);
    std::cout << touchegame << std::endl;
    return (touchegame);
}

arcadeGame::SFMLGame::SFMLGame(void)
{
    std::cout << "SALUT\n" << std::endl;
    window.create(sf::VideoMode(1280, 720), "Arcade Game");
    window.setFramerateLimit(20);
    window.setVerticalSyncEnabled(true);

}

arcadeGame::SFMLGame::~SFMLGame(void)
{

}

std::vector<std::string> arcadeGame::SFMLGame::putWall(std::vector<std::string> Map)
{
    int X = 50;
    int Y = 150;
    for (int i = 0; i < this->line; i++, Y += 25) {
        for (int j = 0; j < this->Column; j++,X += 18) {
            if (Map[i][j] == '/') {
                wall.setPosition(X, Y);
                window.draw(wall);
            }
            if (Map[i][j] == 'O') {
                snake.setPosition(X, Y);
                window.draw(snake);
           }
            if (Map[i][j] == 'o') {
                body.setPosition(X, Y);
                window.draw(body);
           }
            if (Map[i][j] == 'P') {
                food.setPosition(X, Y);
                window.draw(food);
           }
                    if (Map[i][j] == '.') {
                pacgomme.setPosition(X, Y);
                window.draw(pacgomme);
           }
            if (Map[i][j] == '@') {
                body.setPosition(X, Y);
                window.draw(body);
           }
            if (Map[i][j] == '+') {
                pacboule.setPosition(X, Y);
                window.draw(pacboule);
           }
        
            if (Map[i][j] == '-') {
                ghost1.setPosition(X, Y);
                window.draw(ghost1);
           }

            if (Map[i][j] == '=') {
                ghost2.setPosition(X, Y);
                window.draw(ghost2);
           }
            if (Map[i][j] == '&') {
                ghost3.setPosition(X, Y);
                window.draw(ghost3);
           }
            if (Map[i][j] == '"') {
                ghost4.setPosition(X, Y);
                window.draw(ghost4);
           }

        }
        X = 50;
    }
    return (Map);
}

void arcadeGame::SFMLGame::drawwindowGame(void)
{
    window.clear();
    window.draw(sprite);
    window.draw(board);
    window.draw(plato);
    window.draw(score);
}

void arcadeGame::SFMLGame::drawdisplayGame(void)
{
    window.display();
}

int arcadeGame::SFMLGame::init()
{
    if (!sprite_t.loadFromFile("lib_graph/sfml/img/bg3.jpg"))
        std::cout <<"Error with file" << std::endl;
    if (!wall_t.loadFromFile("lib_graph/sfml/img/wall.jpg"))
        std::cout <<"Error with file" << std::endl;
    if (!font2.loadFromFile("lib_graph/sfml/font/futur.ttf"))
        std::cout << "Error loading font" << std::endl;
    if (!food_t.loadFromFile("lib_graph/sfml/img/feed.png"))
        std::cout << "Error loading font" << std::endl;
    if (!snake_t.loadFromFile("lib_graph/sfml/img/head.png"))
        std::cout << "Error loading font" << std::endl;
    if (!body_t.loadFromFile("lib_graph/sfml/img/body.png"))
        std::cout << "Error loading font" << std::endl;
    if (!ghost1_t.loadFromFile("lib_graph/sfml/img/ghost1.png"))
        std::cout << "Error loading font" << std::endl;
    if (!ghost2_t.loadFromFile("lib_graph/sfml/img/ghost2.png"))
        std::cout << "Error loading font" << std::endl;
    if (!ghost3_t.loadFromFile("lib_graph/sfml/img/ghost3.png"))
        std::cout << "Error loading font" << std::endl;
    if (!ghost4_t.loadFromFile("lib_graph/sfml/img/ghost4.png"))
        std::cout << "Error loading font" << std::endl;
    if (!pacboule_t.loadFromFile("lib_graph/sfml/img/pacboule.png"))
        std::cout << "Error loading font" << std::endl;
    if (!pacgomme_t.loadFromFile("lib_graph/sfml/img/pacgomme.png"))
        std::cout << "Error loading font" << std::endl;


    score.setFont(font2);
    score.setString("Score: ");
    score.setCharacterSize(40);
    score.setPosition(1000, 50);

    sprite.setTexture(sprite_t);
    wall.setTexture(wall_t);
    snake.setTexture(snake_t);
    food.setTexture(food_t);
    body.setTexture(body_t);
    ghost1.setTexture(ghost1_t);
    ghost2.setTexture(ghost2_t);
    ghost3.setTexture(ghost3_t);
    ghost4.setTexture(ghost4_t);
    pacboule.setTexture(pacboule_t);
    pacgomme.setTexture(pacgomme_t);

    board.setSize(sf::Vector2f(1200, 80));
    board.setFillColor(sf::Color(0, 0, 0, 150));
    board.setOutlineThickness(5);
    board.setOutlineColor(sf::Color(250, 250, 250));
    board.setPosition(40, 40);

    plato.setSize(sf::Vector2f(1200, 550));
    plato.setFillColor(sf::Color(0, 0, 0, 150));
    plato.setOutlineThickness(5);
    plato.setOutlineColor(sf::Color(250, 250, 250));
    plato.setPosition(40, 140);
    return (0);
}

int arcadeGame::SFMLGame::IsOpenGame(std::vector<std::string> Map)
{
    //exit(0);
    init();
    drawwindowGame();
    stockline(Map);
    Map = putWall(Map);
    this->touchegame = IsKeyPressedGame();
        IsEventGame(Map);
     drawdisplayGame();
    return (this->touchegame);
}

void arcadeGame::SFMLGame::IsCloseWin(void)
{
    window.close();
}

void arcadeGame::SFMLGame::IsCloseGame(void)
{
    window.close();
    exit(EXIT_SUCCESS);
}

void arcadeGame::SFMLGame::IsEventGame(std::vector<std::string> Map)
{
        while (window.pollEvent(event))
        {
            IsKeyPressed();
        }
        if (event.type == sf::Event::Closed) {
            IsCloseGame();
        }
}

int arcadeGame::SFMLGame::IsKeyPressedGame(void) 
{
    if (event.type == sf::Event::KeyPressed)
    {
        if (event.key.code == sf::Keyboard::Left) {
        this->touchegame = 1;
        }
        if (event.key.code == sf::Keyboard::Up) {
        this->touchegame = 2;
        }
        if (event.key.code == sf::Keyboard::Right) {
        this->touchegame = 3;
        }
        if (event.key.code == sf::Keyboard::Down) {
        this->touchegame = 4;
        }
        if (event.key.code == sf::Keyboard::F3) {
        this->touchegame = 55;
        }
        if (event.key.code == sf::Keyboard::N) {
        std::cout << "N" << std::endl;
        }

        if (event.key.code == sf::Keyboard::F4) {
        this->touchegame = 66;
        }
        if (event.key.code == sf::Keyboard::F5) {
        this->touchegame = 77;
        }
        if (event.key.code == sf::Keyboard::F6) {
        this->touchegame = 77;
        }
    }
    return (touchegame);
}

void arcadeGame::SFMLGame::stockline(std::vector<std::string> Map)
{
    this->line = Map.size();
    this->Column = Map[0].size() + 1;
}
