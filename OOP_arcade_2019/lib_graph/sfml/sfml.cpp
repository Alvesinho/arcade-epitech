/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** main (1)
*/

#include "../../include/include.hpp"
#include "../../include/core.hpp"


int arcade::SFMLGraphic::commetuveux(void)
{
    initMenu();
    setTexture();
    if (this->lever == 0) {
    initBox();
    initText();
    }
    initTextSnake();
    initTextPacman();
    this->stock = IsOpen();
    if (this->stock == 2 || this->stock == 3)
        return (this->stock);
    return (this->jeu);
}

arcade::SFMLGraphic::SFMLGraphic(void)
{
  this->window.create(sf::VideoMode(1280, 720), "Arcade");
  this->window.setFramerateLimit(20);
  this->window.setVerticalSyncEnabled(true);
}

arcade::SFMLGraphic::~SFMLGraphic(void)
{

}

void arcade::SFMLGraphic::setTexture(void)
{
    sprite.setTexture(texture);
    snkmenu.setTexture(snkmenu_t);
    pac.setTexture(pacmenu_t);
    snkrules.setTexture(snkrules_t);
    pacrules.setTexture(pacrules_t);
}

void arcade::SFMLGraphic::initMenu(void)
{
    if (!font.loadFromFile("lib_graph/sfml/font/futur.ttf"))
        std::cout << "Error loading font\n";
    if (!texture.loadFromFile("lib_graph/sfml/img/bg3.jpg"))
        std::cout <<"Error with file" << std::endl;
    if (!snkmenu_t.loadFromFile("lib_graph/sfml/img/bg4.jpg"))
        std::cout <<"Error with file" << std::endl;
    if (!pacmenu_t.loadFromFile("lib_graph/sfml/img/bgpacman.jpg"))
        std::cout <<"Error with file" << std::endl;
    if (!snkrules_t.loadFromFile("lib_graph/sfml/img/rulessnake.png"))
        std::cout <<"Error with file" << std::endl;
    if (!pacrules_t.loadFromFile("lib_graph/sfml/img/rulespacman.png"))
        std::cout <<"Error with file" << std::endl;
}

void arcade::SFMLGraphic::initBox(void)
{
        ids1.setSize(sf::Vector2f(183, 50));
        ids1.setFillColor(sf::Color(30, 30, 170, 150));
        ids1.setOutlineThickness(5);
        ids1.setOutlineColor(sf::Color(250, 250, 250));
        ids1.setPosition(20, 650);
        
        ids2.setSize(sf::Vector2f(200, 100));
        ids2.setFillColor(sf::Color(30, 30, 170, 150));
        ids2.setOutlineThickness(5);
        ids2.setOutlineColor(sf::Color(250, 250, 250));
        ids2.setPosition(200, 150);

        ids3.setSize(sf::Vector2f(800, 100));
        ids3.setFillColor(sf::Color(30, 30, 170, 150));
        ids3.setOutlineThickness(5);
        ids3.setOutlineColor(sf::Color(250, 250, 250));
        ids3.setPosition(200, 350);

        ids4.setSize(sf::Vector2f(200, 100));
        ids4.setFillColor(sf::Color(30, 30, 170, 150));
        ids4.setOutlineThickness(5);
        ids4.setOutlineColor(sf::Color(250, 250, 250));
        ids4.setPosition(800, 150);

        ids5.setSize(sf::Vector2f(200, 100));
        ids5.setFillColor(sf::Color(30, 30, 170, 150));
        ids5.setOutlineThickness(5);
        ids5.setOutlineColor(sf::Color(250, 250, 250));
        ids5.setPosition(500, 150);

        snk_play.setSize(sf::Vector2f(500, 100));
        snk_play.setFillColor(sf::Color(30, 30, 170, 150));
        snk_play.setOutlineThickness(5);
        snk_play.setOutlineColor(sf::Color(250, 250, 250));
        snk_play.setPosition(400, 150);

        snk_exit.setSize(sf::Vector2f(500, 100));
        snk_exit.setFillColor(sf::Color(30, 30, 170, 150));
        snk_exit.setOutlineThickness(5);
        snk_exit.setOutlineColor(sf::Color(250, 250, 250));
        snk_exit.setPosition(400, 350);

        snk_rules.setSize(sf::Vector2f(500, 100));
        snk_rules.setFillColor(sf::Color(30, 30, 170, 150));
        snk_rules.setOutlineThickness(5);
        snk_rules.setOutlineColor(sf::Color(250, 250, 250));
        snk_rules.setPosition(400, 550);
    
        snk_play2.setSize(sf::Vector2f(500, 100));
        snk_play2.setFillColor(sf::Color(30, 30, 170, 150));
        snk_play2.setOutlineThickness(5);
        snk_play2.setOutlineColor(sf::Color(250, 250, 250));
        snk_play2.setPosition(400, 150);

        snk_exit2.setSize(sf::Vector2f(500, 100));
        snk_exit2.setFillColor(sf::Color(30, 30, 170, 150));
        snk_exit2.setOutlineThickness(5);
        snk_exit2.setOutlineColor(sf::Color(250, 250, 250));
        snk_exit2.setPosition(400, 350);

        snk_rules2.setSize(sf::Vector2f(500, 100));
        snk_rules2.setFillColor(sf::Color(30, 30, 170, 150));
        snk_rules2.setOutlineThickness(5);
        snk_rules2.setOutlineColor(sf::Color(250, 250, 250));
        snk_rules2.setPosition(400, 550);

}

void arcade::SFMLGraphic::initText(void)
{
        systeme.setFont(font);
        systeme.setString("Name");
        systeme.setCharacterSize(40);
        systeme.setPosition(550,50);
        
        bt_hostname.setFont(font);
        bt_hostname.setString("EXIT");
        bt_hostname.setCharacterSize(40);
        bt_hostname.setPosition(80,650);
        
        bt_systeme.setFont(font);
        bt_systeme.setString("Snake");
        bt_systeme.setCharacterSize(40);
        bt_systeme.setPosition(250,170);
        
        bt_RAM.setFont(font);
        bt_RAM.setString("PacMan");
        bt_RAM.setCharacterSize(40);
        bt_RAM.setPosition(840,170);
        
        bt_HS.setFont(font);
        bt_HS.setString("HightScore");
        bt_HS.setCharacterSize(40);
        bt_HS.setPosition(540,375);        
        
        bt_play.setFont(font);
        bt_play.setString("Play");
        bt_play.setCharacterSize(40);
        bt_play.setPosition(600,175);

        bt_rules.setFont(font);
        bt_rules.setString("Rules");
        bt_rules.setCharacterSize(40);
        bt_rules.setPosition(600,375);

        bt_exit.setFont(font);
        bt_exit.setString("Exit");
        bt_exit.setCharacterSize(40);
        bt_exit.setPosition(600,575);
}

void arcade::SFMLGraphic::initTextSnake(void)
{
        bt_play.setFont(font);
        bt_play.setString("Play");
        bt_play.setCharacterSize(40);
        bt_play.setPosition(600,175);

        bt_rules.setFont(font);
        bt_rules.setString("Rules");
        bt_rules.setCharacterSize(40);
        bt_rules.setPosition(600,375);

        bt_exit.setFont(font);
        bt_exit.setString("Exit");
        bt_exit.setCharacterSize(40);
        bt_exit.setPosition(600,575);
}

void arcade::SFMLGraphic::initTextPacman(void)
{
        bt_play2.setFont(font);
        bt_play2.setString("Play");
        bt_play2.setCharacterSize(40);
        bt_play2.setPosition(600,175);

        bt_rules2.setFont(font);
        bt_rules2.setString("Rules");
        bt_rules2.setCharacterSize(40);
        bt_rules2.setPosition(600,375);

        bt_exit2.setFont(font);
        bt_exit2.setString("Exit");
        bt_exit2.setCharacterSize(40);
        bt_exit2.setPosition(600,575);
}

int arcade::SFMLGraphic::IsOpen(void)
{
    while (window.isOpen())
    {
        IsEvent();
        if (this->stock == 2 || this->stock == 3) {
            return (this->stock);
        }
        window.clear();
        if (this->lever == 0)
            drawmenu();
        if (this->lever == 99) {
        std::ifstream file("highscore.txt", std::ifstream::in);
        std::string buffer;
        std::string str_score;
        std::getline(file, buffer);
        file.close();
        hightscore.setFont(font);
        hightscore.setString(buffer);
        hightscore.setCharacterSize(40);
        hightscore.setPosition(550,50);
        drawhs();
        }
        if (this->lever == 1)
            drawmenusnk();
        if (this->lever == 3)
            drawrulessnk();
        if (this->lever == 11) {
            drawmenupac();
        }
        if (this->lever == 33)
            drawrulespac();
        if (this->lever == 2) {
        this->jeu = 100;
        IsClose();
        }
        if (this->lever == 22) {
        this->jeu = 200;
        IsClose();
        }
        //drawgamesnk();
        drawwindow();
    }
    return (this->stock);
}

void arcade::SFMLGraphic::IsClose(void)
{
    window.close();
}

void arcade::SFMLGraphic::IsEvent(void)
{
            while (window.pollEvent(event))
            {
                if (this->lever == 0)
                ActionMenu();
                if (this->lever == 1)
                ActionSnake();
                if (this->lever == 11)
                ActionPacman();
                IsKeyPressed();
                IsMousePressed();
            }
                if (event.type == sf::Event::Closed)
                    IsClose();
}

void arcade::SFMLGraphic::ActionMenu(void)
{
                if (sf::Mouse::getPosition(window).x >= 200 && sf::Mouse::getPosition(window).x <= 405 && sf::Mouse::getPosition(window).y >= 145 && sf::Mouse::getPosition(window).y <= 255)
                    ids2.setFillColor(sf::Color(250, 250, 250, 150));
                else
                    ids2.setFillColor(sf::Color(30, 30, 170, 150));
                if (sf::Mouse::getPosition(window).x >= 500 && sf::Mouse::getPosition(window).x <= 705 && sf::Mouse::getPosition(window).y >= 145 && sf::Mouse::getPosition(window).y <= 255)
                    ids5.setFillColor(sf::Color(250, 250, 250, 150));
                else
                    ids5.setFillColor(sf::Color(30, 30, 170, 150));
                if (sf::Mouse::getPosition(window).x >= 800 && sf::Mouse::getPosition(window).x <= 1005 && sf::Mouse::getPosition(window).y >= 145 && sf::Mouse::getPosition(window).y <= 255)
                    ids4.setFillColor(sf::Color(250, 250, 250, 150));
                else
                    ids4.setFillColor(sf::Color(30, 30, 170, 150));
                if (sf::Mouse::getPosition(window).x >= 20 && sf::Mouse::getPosition(window).x <= 205 && sf::Mouse::getPosition(window).y >= 650 && sf::Mouse::getPosition(window).y <= 700)
                    ids1.setFillColor(sf::Color(250, 250, 250, 150));
                else
                    ids1.setFillColor(sf::Color(30, 30, 170, 150));
                if (sf::Mouse::getPosition(window).x >= 200 && sf::Mouse::getPosition(window).x <= 1005 && sf::Mouse::getPosition(window).y >= 350 && sf::Mouse::getPosition(window).y <= 450)
                    ids3.setFillColor(sf::Color(250, 250, 250, 150));
                else
                    ids3.setFillColor(sf::Color(30, 30, 170, 150));    
}

void arcade::SFMLGraphic::ActionSnake(void)
{
                    if (sf::Mouse::getPosition(window).x >= 400 && sf::Mouse::getPosition(window).x <= 900 && sf::Mouse::getPosition(window).y >= 150 && sf::Mouse::getPosition(window).y <= 250)
                        snk_play.setFillColor(sf::Color(250, 250, 250, 150));
                    else
                        snk_play.setFillColor(sf::Color(30, 30, 170, 150));
                    if (sf::Mouse::getPosition(window).x >= 400 && sf::Mouse::getPosition(window).x <= 900 && sf::Mouse::getPosition(window).y >= 350 && sf::Mouse::getPosition(window).y <= 450)
                        snk_exit.setFillColor(sf::Color(250, 250, 250, 150));
                    else
                        snk_exit.setFillColor(sf::Color(30, 30, 170, 150));
                    if (sf::Mouse::getPosition(window).x >= 400 && sf::Mouse::getPosition(window).x <= 900 && sf::Mouse::getPosition(window).y >= 550 && sf::Mouse::getPosition(window).y <= 650)
                        snk_rules.setFillColor(sf::Color(250, 250, 250, 150));
                    else
                        snk_rules.setFillColor(sf::Color(30, 30, 170, 150));
}

void arcade::SFMLGraphic::ActionPacman(void)
{
                    if (sf::Mouse::getPosition(window).x >= 400 && sf::Mouse::getPosition(window).x <= 900 && sf::Mouse::getPosition(window).y >= 150 && sf::Mouse::getPosition(window).y <= 250)
                        snk_play2.setFillColor(sf::Color(250, 250, 250, 150));
                    else
                        snk_play2.setFillColor(sf::Color(30, 30, 170, 150));
                    if (sf::Mouse::getPosition(window).x >= 400 && sf::Mouse::getPosition(window).x <= 900 && sf::Mouse::getPosition(window).y >= 350 && sf::Mouse::getPosition(window).y <= 450)
                        snk_exit2.setFillColor(sf::Color(250, 250, 250, 150));
                    else
                        snk_exit2.setFillColor(sf::Color(30, 30, 170, 150));
                    if (sf::Mouse::getPosition(window).x >= 400 && sf::Mouse::getPosition(window).x <= 900 && sf::Mouse::getPosition(window).y >= 550 && sf::Mouse::getPosition(window).y <= 650)
                        snk_rules2.setFillColor(sf::Color(250, 250, 250, 150));
                    else
                        snk_rules2.setFillColor(sf::Color(30, 30, 170, 150));
}


void arcade::SFMLGraphic::IsKeyPressed(void) 
{
    if (event.type == sf::Event::KeyPressed)
    {
        if (event.key.code == sf::Keyboard::Escape)
        this->lever = 0;
        if (event.key.code == sf::Keyboard::N)
        this->stock = 2;
        if (event.key.code == sf::Keyboard::P) {
        this->stock = 3;
        return;
        }
    }
}

void arcade::SFMLGraphic::IsMousePressed(void)
{
    if (event.type == sf::Event::MouseButtonPressed)
        {
            if (event.mouseButton.button == sf::Mouse::Left)
                {
                    if (this->lever == 0)
                    {
                        if (event.mouseButton.x >= 200 && event.mouseButton.x <= 405 && event.mouseButton.y >= 145 && event.mouseButton.y <= 255)
                        {
                            this->lever = 1;
                        }
                        if (event.mouseButton.x >= 800 && event.mouseButton.x <= 1005 && event.mouseButton.y >= 145 && event.mouseButton.y <= 255)
                        {
                            this->lever = 11;
                        }

                        if (event.mouseButton.x >= 200 && event.mouseButton.x <= 1005 && event.mouseButton.y >= 350 && event.mouseButton.y <= 450)
                        {
                            this->lever = 99;
                        }

                          if (event.mouseButton.x >= 20 && event.mouseButton.x <= 205 && event.mouseButton.y >= 650 && event.mouseButton.y <= 700)
                        {
                            exit (0);
                        }

                    }
            if (this->lever == 1)
            {
                if (event.mouseButton.x >= 400 && event.mouseButton.x <= 900 && event.mouseButton.y >= 150 && event.mouseButton.y <= 250)
                    {
                        this->lever = 2;
                    }
                if (event.mouseButton.x >= 400 && event.mouseButton.x <= 900 && event.mouseButton.y >= 350 && event.mouseButton.y <= 450)
                    {
                        this->lever = 3;
                    }

                if (event.mouseButton.x >= 400 && event.mouseButton.x <= 900 && event.mouseButton.y >= 550 && event.mouseButton.y <= 850)
                    {
                        exit (0);
                    }   
            }
            if (this->lever == 11)
            {
                if (event.mouseButton.x >= 400 && event.mouseButton.x <= 900 && event.mouseButton.y >= 150 && event.mouseButton.y <= 250)
                    {
                        this->lever = 22;
                    }
                if (event.mouseButton.x >= 400 && event.mouseButton.x <= 900 && event.mouseButton.y >= 350 && event.mouseButton.y <= 450)
                    {
                        this->lever = 33;
                    }

                if (event.mouseButton.x >= 400 && event.mouseButton.x <= 900 && event.mouseButton.y >= 550 && event.mouseButton.y <= 850)
                    {
                        exit (0);
                    }   
            }
            }
        }
}

void arcade::SFMLGraphic::drawmenu(void)
{
            window.draw(sprite);
            window.draw(ids1);
            window.draw(ids2);
            window.draw(ids3);
            window.draw(ids4);
            window.draw(ids5);
            window.draw(systeme);
            window.draw(bt_hostname);
            window.draw(bt_systeme);
            window.draw(bt_time);
            window.draw(bt_RAM);
            window.draw(bt_HS);
}

void arcade::SFMLGraphic::drawmenusnk(void)
{
            window.draw(snkmenu);
            window.draw(snk_play);
            window.draw(snk_rules);
            window.draw(snk_exit);
            window.draw(bt_play);
            window.draw(bt_rules);
            window.draw(bt_exit);
}

void arcade::SFMLGraphic::drawmenupac(void)
{
            window.draw(pac);
            window.draw(snk_play2);
            window.draw(snk_rules2);
            window.draw(snk_exit2);
            window.draw(bt_play2);
            window.draw(bt_rules2);
            window.draw(bt_exit2);
}

void arcade::SFMLGraphic::drawrulessnk(void)
{
            window.draw(snkrules);
}

void arcade::SFMLGraphic::drawrulespac(void)
{
            window.draw(pacrules);
}

void arcade::SFMLGraphic::drawhs(void)
{
            window.draw(sprite);
            window.draw(hightscore);
}

void arcade::SFMLGraphic::drawwindow(void)
{
    window.display();
}
