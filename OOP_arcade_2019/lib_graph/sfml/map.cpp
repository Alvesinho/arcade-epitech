/*
** EPITECH PROJECT, 2020
** Map Pacman
** File description:
** Inho
*/

#include "../../include/map.hpp"
#include "../../include/core.hpp"

int arcadeGamePac::SFMLGamePac::JeuContinuePac(std::vector<std::string> Map)
{
    IsOpenGamePac(Map);
    return (touchegamePac);
}
int arcadeGamePac::SFMLGamePac::GameSnakePac(std::vector<std::string> Map)
{
    IsOpenGamePac(Map);
    return (touchegamePac);
}

arcadeGamePac::SFMLGamePac::SFMLGamePac(void)
{
  printf("ICI\n");

  this->window.create(sf::VideoMode(1280, 720), "WIN");
  this->window.setFramerateLimit(20);
  this->window.setVerticalSyncEnabled(true);
  printf("ICI\n");
}

arcadeGamePac::SFMLGamePac::~SFMLGamePac(void)
{

}

std::vector<std::string> arcadeGamePac::SFMLGamePac::putWallPac(std::vector<std::string> Map)
{
    int X = 50;
    int Y = 150;
    for (int i = 0; i < this->line; i++, Y += 25) {
        for (int j = 0; j < this->Column; j++,X += 18) {
            if (Map[i][j] == '/') {
                wall.setPosition(X, Y);
                window.draw(wall);
                //drawdisplayGame();
            }
            if (Map[i][j] == '.') {
                pacgomme.setPosition(X, Y);
                window.draw(pacgomme);
           }
            if (Map[i][j] == '@') {
                body.setPosition(X, Y);
                window.draw(body);
           }
            if (Map[i][j] == '+') {
                pacboule.setPosition(X, Y);
                window.draw(pacboule);
           }
        
            if (Map[i][j] == '-') {
                ghost1.setPosition(X, Y);
                window.draw(ghost1);
           }

            if (Map[i][j] == '=') {
                ghost2.setPosition(X, Y);
                window.draw(ghost2);
           }
            if (Map[i][j] == '&') {
                ghost3.setPosition(X, Y);
                window.draw(ghost3);
           }
            if (Map[i][j] == '"') {
                ghost4.setPosition(X, Y);
                window.draw(ghost4);
           }


        }
        X = 50;
    }
    return (Map);
}

void arcadeGamePac::SFMLGamePac::drawwindowGamePac(void)
{
    window.clear();
    window.draw(win);
}

void arcadeGamePac::SFMLGamePac::drawdisplayGamePac(void)
{
    window.display();
}

int arcadeGamePac::SFMLGamePac::initPac()
{
    if (!sprite_t.loadFromFile("lib_graph/sfml/img/bg3.jpg"))
        std::cout <<"Error with file" << std::endl;
    if (!wall_t.loadFromFile("lib_graph/sfml/img/wall.jpg"))
        std::cout <<"Error with file" << std::endl;
    if (!font2.loadFromFile("lib_graph/sfml/font/futur.ttf"))
        std::cout << "Error loading font" << std::endl;
    if (!food_t.loadFromFile("lib_graph/sfml/img/feed.png"))
        std::cout << "Error loading font" << std::endl;
    if (!snake_t.loadFromFile("lib_graph/sfml/img/head.png"))
        std::cout << "Error loading font" << std::endl;
    if (!body_t.loadFromFile("lib_graph/sfml/img/body.png"))
        std::cout << "Error loading font" << std::endl;
    if (!ghost1_t.loadFromFile("lib_graph/sfml/img/ghost1.png"))
        std::cout << "Error loading font" << std::endl;
    if (!ghost2_t.loadFromFile("lib_graph/sfml/img/ghost2.png"))
        std::cout << "Error loading font" << std::endl;
    if (!ghost3_t.loadFromFile("lib_graph/sfml/img/ghost3.png"))
        std::cout << "Error loading font" << std::endl;
    if (!ghost4_t.loadFromFile("lib_graph/sfml/img/ghost4.png"))
        std::cout << "Error loading font" << std::endl;
    if (!pacboule_t.loadFromFile("lib_graph/sfml/img/pacboule.png"))
        std::cout << "Error loading font" << std::endl;
    if (!pacgomme_t.loadFromFile("lib_graph/sfml/img/pacgomme.png"))
        std::cout << "Error loading font" << std::endl;
    if (!win_t.loadFromFile("lib_graph/sfml/img/winn.png"))
        std::cout << "Error loading font" << std::endl;


    score.setFont(font2);
    score.setString("Score: ");
    score.setCharacterSize(40);
    score.setPosition(1000, 50);

    sprite.setTexture(sprite_t);
    wall.setTexture(wall_t);
    snake.setTexture(snake_t);
    food.setTexture(food_t);
    body.setTexture(body_t);
    ghost1.setTexture(ghost1_t);
    ghost2.setTexture(ghost2_t);
    ghost3.setTexture(ghost3_t);
    ghost4.setTexture(ghost4_t);
    pacboule.setTexture(pacboule_t);
    pacgomme.setTexture(pacgomme_t);
    win.setTexture(win_t);

    board.setSize(sf::Vector2f(1200, 80));
    board.setFillColor(sf::Color(0, 0, 0, 150));
    board.setOutlineThickness(5);
    board.setOutlineColor(sf::Color(250, 250, 250));
    board.setPosition(40, 40);

    plato.setSize(sf::Vector2f(1200, 550));
    plato.setFillColor(sf::Color(0, 0, 0, 150));
    plato.setOutlineThickness(5);
    plato.setOutlineColor(sf::Color(250, 250, 250));
    plato.setPosition(40, 140);
    return (0);
}

int arcadeGamePac::SFMLGamePac::IsOpenGamePac(std::vector<std::string> Map)
{
    initPac();
    drawwindowGamePac();
    this->touchegamePac = IsKeyPressedGamePac();
    IsEventGamePac(Map);
    drawdisplayGamePac();
    return (this->touchegamePac);
}

void arcadeGamePac::SFMLGamePac::IsCloseWinPac(void)
{
    window.close();
}

void arcadeGamePac::SFMLGamePac::IsCloseGamePac(void)
{
    window.close();
    exit(EXIT_SUCCESS);
}

void arcadeGamePac::SFMLGamePac::IsEventGamePac(std::vector<std::string> Map)
{
        while (window.pollEvent(event))
        {
            //drawwindowGame();
            //putWall(Map);
            //drawdisplayGame();
            //stockline(Map);
            //Map = putWall(Map);
            IsKeyPressed();
        }
        if (event.type == sf::Event::Closed)
           IsCloseGamePac();
}

int arcadeGamePac::SFMLGamePac::IsKeyPressedGamePac(void) 
{
    if (event.type == sf::Event::KeyPressed)
    {
        if (event.key.code == sf::Keyboard::Left) {
        this->touchegamePac = 1;
        }
        if (event.key.code == sf::Keyboard::Up) {
        this->touchegamePac = 2;
        }
        if (event.key.code == sf::Keyboard::Right) {
        this->touchegamePac = 3;
        }
        if (event.key.code == sf::Keyboard::Down) {
        this->touchegamePac = 4;
        }
        if (event.key.code == sf::Keyboard::F3) {
        this->touchegamePac = 555;
        }
        if (event.key.code == sf::Keyboard::F4) {
        this->touchegamePac = 666;
        }
        if (event.key.code == sf::Keyboard::F5) {
        this->touchegamePac = 777;
        }
        if (event.key.code == sf::Keyboard::F6) {
        this->touchegamePac = 777;
        }
    }
    return (touchegamePac);
}

void arcadeGamePac::SFMLGamePac::stocklinePac(std::vector<std::string> Map)
{
    this->line = Map.size();
    this->Column = Map[0].size() + 1;
}
