/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** pacman_ncurses
*/

#include "../../include/ncurses_menu.hpp"
#include "../../include/ncurses_snake.hpp"
#include "../../include/ncurses_pacman.hpp"
#include "../../include/core.hpp"
#include "../../include/pacman.hpp"
#include <ncurses.h>
#include <curses.h>

int pacman_ncurses::map_ncurses_pacman(std::vector<std::string> Map)
{
    int ret = 0;
    for (size_t i = 0; i < Map.size(); i++) {
        for (size_t j = 0; j < Map[i].length(); j++) {
            if (Map[i][j] == '.') {
                Map[i][j] = '*';
            }
        }
         mvwprintw(this->pacman_win,i + 1,2, Map[i].c_str());
    }
    wrefresh(this->pacman_win);
    return (ret);
}

int pacman_ncurses::game_loop_pacman(std::vector<std::string> Map) 
{
    int key = 0;
    int dir = 0;
    bool quit = false;
    char buffer[200];

    Pac::pacman pacman;
    //pacman.score = 0;
    std::vector<std::string> MapPacman;
    pacman.read_highscore_pacman();
    MapPacman = Map;
    initscr();
    this->pacman_win = newwin(30, 80, 0, 0);
    box(this->pacman_win, 0 ,0);
    noecho();
    keypad(this->pacman_win, TRUE );
    curs_set( 0 );
    nodelay(this->pacman_win,1);
    while (quit == false) {
        map_ncurses_pacman(MapPacman);
        usleep(80000);
        key = wgetch(this->pacman_win);
        switch (key) {
            case KEY_UP:
                dir = 2;
                break;
            case KEY_LEFT:
                dir = 1;
                break;
            case KEY_RIGHT:
                dir = 3;
                break;
            case KEY_DOWN:
                dir = 4; 
                break;
            case 'q':
                quit = true;
                break;
            case 'p':
                dir = 6;
                this->lib = 1;
                this->game = 0;
                quit = true;
                break;
            case 'n':
                dir = 5;
                this->lib = 3;
                this->game = 0;
                quit = true;
                break;
            case 'x':
                dir = 10;
                this->lib = 2;
                this->game = 2;
                quit = true;
                break;
        }
        MapPacman = pacman.beginPacman(MapPacman, dir);
       // if(snake.Exit == 1)
         //   quit = true;
        sprintf(buffer, "score: %f", pacman.score);
        mvwprintw(this->pacman_win, 5, 50, buffer);
        sprintf(buffer, "highscore: %f", pacman.highscore);
        mvwprintw(this->pacman_win, 8, 50., buffer);
        //snake.game_win();
    }
    pacman.write_highscore_pacman();
    delwin(this->pacman_win);
    endwin();
   return (dir);
}
