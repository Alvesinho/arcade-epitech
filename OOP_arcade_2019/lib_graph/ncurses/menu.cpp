/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** menu
*/

#include <ncurses.h>
#include <curses.h>
#include "../../include/ncurses_menu.hpp"
#include "../../include/ncurses_snake.hpp"
#include "../../include/core.hpp"
#include "../../include/GameSnake.hpp"

menu_ncurses::menu_ncurses()
{
}

menu_ncurses::~menu_ncurses()
{
}


int menu_ncurses::ncurses_menu() 
{
    char list[6][18] = { "Pseudo", "Pacman", "Snake", "Hightscore_pacman", "Hightscore_snake", "Exit" };
    char item[18];
    int ch, i = 0;
    int ret = 0;
    bool quit = false;
 
    initscr();
    w = newwin( 10, 60, 1, 1 );
    box( w, 0, 0 ); 
    for( i=0; i<6; i++ ) {
        if( i == 0 ) 
            wattron( w, A_STANDOUT );
        else
            wattroff( w, A_STANDOUT );
        sprintf(item, "%-7s",  list[i]);
        mvwprintw( w, i + 1, LINES / 2, "%s", item );
    }
    wrefresh( w ); 
    i = 0;
    noecho(); 
    keypad( w, TRUE ); 
    curs_set( 0 ); 
    while(quit == false){ 
            ch = wgetch(w) ;
            sprintf(item, "%-7s",  list[i]); 
            mvwprintw( w, i+1, LINES / 2, "%s", item );
            switch( ch ) {
                case KEY_UP:
                        i--;
                        i = (i < 0) ? 5 : i;
                        break;
                case KEY_DOWN:
                        i++;
                        i = (i > 5) ? 0 : i;
                        break;
                case 10:
                    if (i == 1) {
                        quit = true;
                        delwin(w);
                        endwin();
                        this->pacman_menu();
                        return (ret);
                    }
                    if (i == 2) {
                        quit = true;
                        delwin(w);
                        endwin();
                        this->snake_menu();
                        return (ret);
                    }
                    if (i == 4) {
                        initscr();
                        noecho();
                        curs_set(0);
                        this->highscore_snake = newwin(10, 60, 1, 1);
                        box(this->highscore_snake, 0, 0 );
                        std::ifstream file("highscore.txt", std::ifstream::in);
                        std::string buffer;
                        int j = 0;
                        std::getline(file, buffer);
                        while (file.good()) {
                            mvwprintw(this->highscore_snake, 2 + j, 2, buffer.c_str());
                            std::getline(file, buffer);
                            j++;
                        }
                        wrefresh(this->highscore_snake);
                        file.close();
                        wgetch(this->highscore_snake);
                        wclear(this->highscore_snake);
                        wrefresh(this->highscore_snake);
                        delwin(this->highscore_snake);
                        endwin();
                        box(this->w, 0, 0);
                    }
                    if (i == 5) {
                        quit = true;
                        delwin(w);
                        endwin();
                        exit(EXIT_SUCCESS);
                        return (ret);
                    }
                    break;
                case 'p':
                    this->game = 0;
                    this->lib = 1;
                    ret = 6;
                    quit = true;
                    break;
                case 'n':
                    this->game = 0;
                    this->lib = 3;
                    ret = 5;
                    quit = true;
                    break;
                 case 'q':
                    ret = 0;
                    quit = true;
                    break;
            }
            wattron( w, A_STANDOUT );
            sprintf(item, "%-7s",  list[i]);
            mvwprintw( w, i+1, LINES / 2, "%s", item);
            wattroff( w, A_STANDOUT );
    }
    delwin(w);
    endwin();
    return(ret);
}

int menu_ncurses::snake_menu()
{
    snake_ncurses snake_s;
    std::vector<std::string> Map;

    char list[4][11] = { "Play", "BackToMenu", "Rules", "Exit"};
    char item[11];
    int ch;
    int i = 0;
    int ret = 0;
    bool quit = false;

    initscr();
    this->menu_snake = newwin(10 , 60, 1, 1);
    box(this->menu_snake, 0, 0);

    for (i = 0; i < 4; i++) {
        if (i == 0)
            wattron(this->menu_snake, A_STANDOUT );
         else
            wattroff(this->menu_snake, A_STANDOUT );
        sprintf(item, "%-7s",  list[i]);
        mvwprintw(this->menu_snake, i + 1, LINES / 2, "%s", item );
    }
    wrefresh(this->menu_snake);
     i = 0;
    noecho();
    keypad(this->menu_snake, TRUE );
    curs_set( 0 );
     while(quit == false ) { 
            ch = wgetch(this->menu_snake);
            sprintf(item, "%-7s",  list[i]); 
            mvwprintw(this->menu_snake, i + 1, LINES / 2, "%s", item ); 
            switch (ch) {
                case KEY_UP:
                    i--;
                    i = (i < 0 ) ? 3 : i;
                    break;
                case KEY_DOWN:
                    i++;
                    i = (i > 3 ) ? 0 : i;
                    break;
                case 10:
                    if (i == 1) {
                        quit = true;
                        delwin(this->menu_snake);
                        this->ncurses_menu();
                        return (ret);
                }
                    if (i == 0) {
                        quit = true;
                        delwin(this->menu_snake);
                        endwin();
                        this->lib = 2;
                        this->game =1;
                        return (ret);
                    }
                    if (i == 2) {
                        initscr();
                        noecho();
                        curs_set(0);
                        this->rules_snake = newwin(30, 100, 1, 1);
                        box(this->rules_snake, 0, 0 );
                        std::ifstream file("rules-snake.txt", std::ifstream::in);
                        std::string buffer;
                        int j = 0;
                        std::getline(file, buffer);
                        while (file.good()) {
                            mvwprintw(this->rules_snake, 2 + j, 2, buffer.c_str());
                            std::getline(file, buffer);
                            j++;
                        }
                        wrefresh(this->rules_snake);
                        file.close();
                        wgetch(this->rules_snake);
                        wclear(this->rules_snake);
                        wrefresh(this->rules_snake);
                        delwin(this->rules_snake);
                        endwin();
                        box(this->menu_snake, 0, 0);
                    }
                    if (i == 3) {
                        quit = true;
                        delwin(this->menu_snake);
                        endwin();
                        exit(EXIT_SUCCESS);
                        return (ret);
                    }
                 break;
                case 'p':
                    ret = 6;
                    this->lib = 1;
                    this->game = 0;
                    quit = true;
                    break;
                case 'n':
                    this->lib = 3;
                    this->game = 0;
                    ret = 5;
                    quit = true;
                    break;
                case 'q':
                    ret = 0;
                    quit = true;
                    break;
            }
            wattron(this->menu_snake, A_STANDOUT );
            sprintf(item, "%-7s",  list[i]);
            mvwprintw(this->menu_snake, i + 1, LINES / 2, "%s", item);
            wattroff(menu_snake, A_STANDOUT );
        }
    delwin(this->menu_snake);
    endwin();
    return (ret);
}