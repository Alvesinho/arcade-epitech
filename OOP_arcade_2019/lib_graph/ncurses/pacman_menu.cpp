/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** pacman_menu
*/

#include <ncurses.h>
#include <curses.h>
#include "../../include/ncurses_menu.hpp"
#include "../../include/ncurses_snake.hpp"
#include "../../include/core.hpp"
#include "../../include/GameSnake.hpp"

int menu_ncurses::pacman_menu()
{
    snake_ncurses snake_s;
    std::vector<std::string> Map;

    char list[4][11] = { "Play", "BackToMenu", "Rules", "Exit"};
    char item[11];
    int ch;
    int i = 0;
    int ret = 0;
    bool quit = false;

    initscr();
    this->menu_pacman = newwin(10 , 60, 1, 1);
    box(this->menu_pacman, 0, 0);

    for (i = 0; i < 4; i++) {
        if (i == 0)
            wattron(this->menu_pacman, A_STANDOUT );
         else
            wattroff(this->menu_pacman, A_STANDOUT );
        sprintf(item, "%-7s",  list[i]);
        mvwprintw(this->menu_pacman, i + 1, LINES / 2, "%s", item );
    }
    wrefresh(this->menu_pacman);
     i = 0;
    noecho();
    keypad(this->menu_pacman, TRUE );
    curs_set( 0 );
     while(quit == false ) { 
            ch = wgetch(this->menu_pacman);
            sprintf(item, "%-7s",  list[i]); 
            mvwprintw(this->menu_pacman, i + 1, LINES / 2, "%s", item ); 
            switch (ch) {
                case KEY_UP:
                    i--;
                    i = (i < 0 ) ? 3 : i;
                    break;
                case KEY_DOWN:
                    i++;
                    i = (i > 3 ) ? 0 : i;
                    break;
                case 10:
                    if (i == 1) {
                        quit = true;
                        delwin(this->menu_pacman);
                        this->ncurses_menu();
                        return (ret);
                }
                    if (i == 0) {
                        quit = true;
                        delwin(this->menu_pacman);
                        endwin();
                        this->lib = 2;
                        this->game = 2;
                        return (ret);
                    }
                    if (i == 2) {
                        initscr();
                        noecho();
                        curs_set(0);
                        this->rules_pacman = newwin(30, 100, 1, 1);
                        box(this->rules_pacman, 0, 0 );
                        std::ifstream file("rules-pacman.txt", std::ifstream::in);
                        std::string buffer;
                        int j = 0;
                        std::getline(file, buffer);
                        while (file.good()) {
                            mvwprintw(this->rules_pacman, 2 + j, 2, buffer.c_str());
                            std::getline(file, buffer);
                            j++;
                        }
                        wrefresh(this->rules_pacman);
                        file.close();
                        wgetch(this->rules_pacman);
                        wclear(this->rules_pacman);
                        wrefresh(this->rules_pacman);
                        delwin(this->rules_pacman);
                        endwin();
                        box(this->menu_pacman, 0, 0);
                    }
                    if (i == 3) {
                        quit = true;
                        delwin(this->menu_pacman);
                        endwin();
                        exit(EXIT_SUCCESS);
                        return (ret);
                    }
                 break;
                case 'p':
                    ret = 6;
                    this->lib = 1;
                    this->game = 0;
                    quit = true;
                    break;
                case 'n':
                    this->lib = 3;
                    this->game = 0;
                    ret = 5;
                    quit = true;
                    break;
                case 'q':
                    ret = 0;
                    quit = true;
                    break;
            }
            wattron(this->menu_pacman, A_STANDOUT );
            sprintf(item, "%-7s",  list[i]);
            mvwprintw(this->menu_pacman, i + 1, LINES / 2, "%s", item);
            wattroff(this->menu_pacman, A_STANDOUT );
        }
    delwin(this->menu_pacman);
    endwin();
    return (ret);
}