/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** SDL_MenuSnak
*/

#include "../../include/SDL_MenuSnak.hpp"
#include "../../include/SDL.hpp"
#include "../../include/SDL_Snake.hpp"
#include "../../include/core.hpp"

SDL_MenuSnak::SDL_MenuSnak()
{
}

int SDL_MenuSnak::changeToMouseSnake()
{
    if (SDL::event.motion.x > 230 && SDL::event.motion.x < 475
        && SDL::event.motion.y > 140 && SDL::event.motion.y < 190) {
            SDL_BlitSurface(SDL_MenuSnak::playBlanc, NULL, SDL::ecran, &positionFond);
            SDL_Flip(SDL::ecran);
            if (event.button.button == SDL_BUTTON_LEFT) {
                return (4);
            }
        }
    else if (SDL::event.motion.x > 230 && SDL::event.motion.x < 475
        && SDL::event.motion.y > 235 && SDL::event.motion.y < 285) {
            SDL_BlitSurface(SDL_MenuSnak::Rules, NULL, SDL::ecran, &positionFond);
            SDL_Flip(SDL::ecran);
        }
    else if (SDL::event.motion.x > 230 && SDL::event.motion.x < 475
        && SDL::event.motion.y > 315 && SDL::event.motion.y < 370) {
            SDL_BlitSurface(SDL::ExitBlanc, NULL, SDL::ecran, &positionFond);
            SDL_Flip(SDL::ecran);
            if (event.button.button == SDL_BUTTON_LEFT) {
                freeSpace_snake();
                SDL_Quit();
                exit(EXIT_SUCCESS);
            }
        }
    else if (SDL::event.motion.x > 450 && SDL::event.motion.x < 705
        && SDL::event.motion.y > 400 && SDL::event.motion.y < 460) {
            SDL_BlitSurface(SDL_MenuSnak::mainMenu, NULL, SDL::ecran, &positionFond);
            SDL_Flip(SDL::ecran);
        if (event.button.button == SDL_BUTTON_LEFT) {
                return (3);
            }
        }
    else {
        SDL_BlitSurface(SDL_MenuSnak::background_menuSnake, NULL, SDL::ecran, &positionFond);
        SDL_Flip(SDL::ecran);
    }
    return (0);
}

void SDL_MenuSnak::init_image_snake()
{
    SDL_MenuSnak::background_menuSnake = SDL_LoadBMP("lib_graph/sdl1/img/menu_snake.bmp");
    SDL_MenuSnak::playBlanc = SDL_LoadBMP("lib_graph/sdl1/img/snake_play_blanc.bmp");
    SDL_MenuSnak::rulesBlanc = SDL_LoadBMP("lib_graph/sdl1/img/snake_rules_blanc.bmp");
    SDL::ExitBlanc = SDL_LoadBMP("lib_graph/sdl1/img/snake_exit_blanc.bmp");
    SDL_MenuSnak::mainMenu = SDL_LoadBMP("lib_graph/sdl1/img/snake_mainmenu_blanc.bmp");
    SDL_MenuSnak::Rules = SDL_LoadBMP("lib_graph/sdl1/img/RulesSDLSnake.bmp");
}

void SDL_MenuSnak::blit_to_surface_snake()
{
    SDL_BlitSurface(SDL_MenuSnak::background_menuSnake, NULL, SDL::ecran, &positionFond);
}

void SDL_MenuSnak::flip_ecran_snake()
{
    SDL_Flip(SDL::ecran);
}

int SDL_MenuSnak::pause_snake()
{
    int continuer = 1;

    while (continuer) {
        SDL_PollEvent(&event);
        this->stockNb = changeToMouseSnake();
        switch(SDL::event.type)
        {
            case SDL_QUIT:
                freeSpace_snake();
                SDL_Quit();
                exit(EXIT_SUCCESS);
                break;
            case SDL_MOUSEMOTION:
                break;
            case SDL_MOUSEBUTTONUP:
                break;
            case SDL_KEYDOWN:
            switch (SDL::event.key.keysym.sym)
            {
                case SDLK_n:
                    this->stockNb = 1;
                    continuer = 0;
                    break;
                case SDLK_p:
                    this->stockNb = 2;
                    continuer = 0;
                    break;
                default:
                    SDL_FillRect(SDL::ecran,NULL,SDL_MapRGB(SDL::ecran->format,140,140,140));
                    break;
            }
            break;
            default:
                    SDL_FillRect(SDL::ecran,NULL,SDL_MapRGB(SDL::ecran->format,140,140,140));
                    break;
        break;
        }
        if (this->stockNb == 3)
            return(3);
        if (this->stockNb == 4)
            return (4);
    }
    return(this->stockNb);
}

void SDL_MenuSnak::freeSpace_snake()
{
    SDL_FreeSurface(SDL_MenuSnak::background_menuSnake); /* On libère la surface */
    SDL_FreeSurface(SDL::ExitBlanc); /* On libère la surface */
    SDL_FreeSurface(SDL_MenuSnak::playBlanc); /* On libère la surface */
    SDL_FreeSurface(SDL_MenuSnak::rulesBlanc); /* On lière la surface */
    SDL_FreeSurface(SDL_MenuSnak::mainMenu); /* On lière la surface */
    SDL_FreeSurface(SDL_MenuSnak::Rules); /* On lière la surface */
}

SDL_MenuSnak::~SDL_MenuSnak()
{
}
