/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** pacman
*/

#include "../../include/pacman.hpp"
#include "../../include/core.hpp"

Pac::pacman::pacman(void)
{
}

void Pac::pacman::read_highscore_pacman()
{
  std::ifstream file("highscore_pacman.txt", std::ifstream::in);
  std::string buffer;
  std::string str_score;
  std::getline(file, buffer);
  int pos = buffer.find_last_of(' ');
  file.close();
  str_score = buffer.substr(pos);
  this->highscore = atoi(str_score.c_str());
}

void Pac::pacman::write_highscore_pacman()
{
    if (this->score > this->highscore) {
        this->highscore = this->score;
        std::ofstream file("highscore_pacman.txt", std::ofstream::out);
        std::string buffer;
        buffer = "PSEUDO " + std::to_string(this->highscore);
        std::cout << buffer << std::endl;
        file << buffer;
        file.close();
    }
}


std::vector<std::string> Pac::pacman::beginPacman(std::vector<std::string> MapPacman, int touche)
{
    this->touchePressed = touche;
    for (size_t i = 0; i < MapPacman.size(); i++) {
        for (size_t j = 0; j < MapPacman[0].size(); j++) {
            if (MapPacman[i][j] == '@') {
                this->posX = i;
                this->posY = j;
            }
        }
    }
    if (this->touchePressed == 1) {
        if (MapPacman[this->posX][this->posY - 1] == '&' || MapPacman[this->posX][this->posY - 1] == '=' 
            || MapPacman[this->posX][this->posY - 1] == '"' || MapPacman[this->posX][this->posY - 1] == '-') {
                core c;
                c.game_over_Pacman(this->score);
            }
        if (MapPacman[this->posX][this->posY - 1] == 'R') {
            MapPacman[this->posX][this->posY] = '*';
            this->posY = MapPacman[0].size() - 2;
            MapPacman[this->posX][this->posY] = '@';
        }
        else if (MapPacman[this->posX][this->posY - 1] == '.')
                this->score ++;
        else if (MapPacman[this->posX][this->posY - 1] == '.')
                this->score += 0.5;
        if ((MapPacman[this->posX][this->posY - 1] != '/')) {
                MapPacman[this->posX][this->posY - 1] = '@';
                MapPacman[this->posX][this->posY] = ' ';
            }
    }
    if (this->touchePressed == 2) {
        if (MapPacman[this->posX - 1][this->posY] == '&' || MapPacman[this->posX - 1][this->posY] == '=' 
            || MapPacman[this->posX - 1][this->posY] == '"' || MapPacman[this->posX - 1][this->posY] == '-') {
                core c;
                c.game_over_Pacman(this->score);
            }
            if (MapPacman[this->posX - 1][this->posY] == '.')
                this->score ++;
            else if (MapPacman[this->posX - 1][this->posY] == '+')
                this->score += 0.5;
            if ((MapPacman[this->posX - 1][this->posY] != '/')) {
                MapPacman[this->posX - 1][this->posY] = '@';
                MapPacman[this->posX][this->posY] = ' ';
            }
    }
    if (this->touchePressed == 3) {
        if (MapPacman[this->posX][this->posY + 1] == '&' || MapPacman[this->posX][this->posY + 1] == '=' 
            || MapPacman[this->posX][this->posY + 1] == '"' || MapPacman[this->posX][this->posY + 1] == '-') {
                core c;
                c.game_over_Pacman(this->score);
            }
        if (MapPacman[this->posX][this->posY + 1] == 'R') {
            MapPacman[this->posX][this->posY] = '*';
            this->posY = 1;
            MapPacman[this->posX][this->posY] = '@';
        }
        else if (MapPacman[this->posX][this->posY + 1] == '.')
                this->score ++;
        else if (MapPacman[this->posX][this->posY + 1] == '+')
                this->score +=0.5;
        if ((MapPacman[this->posX][this->posY + 1] != '/')) {
            MapPacman[this->posX][this->posY + 1] = '@';
            MapPacman[this->posX][this->posY] = ' ';
        }
    }
    if (this->touchePressed == 4) {
        if (MapPacman[this->posX + 1][this->posY] == '&' || MapPacman[this->posX +1][this->posY] == '=' 
            || MapPacman[this->posX + 1][this->posY] == '"' || MapPacman[this->posX + 1][this->posY] == '-') {
                core c;
                c.game_over_Pacman(this->score);
            }
            if (MapPacman[this->posX + 1][this->posY] == '.')
                this->score ++;
            else if (MapPacman[this->posX + 1][this->posY] == '.')
                this->score += 0.5;
        if ((MapPacman[this->posX + 1][this->posY] != '/')) {
            MapPacman[this->posX + 1][this->posY] = '@';
            MapPacman[this->posX][this->posY] = ' ';
        }
    }
    for (size_t i = 0; i < MapPacman.size(); i++) {
        for (size_t j = 0; j < MapPacman[0].size(); j++) {
        }
    }
    //std::cout << "score = " << this->score<< std::endl;
    return (MapPacman);
}

Pac::pacman::~pacman(void)
{
}
