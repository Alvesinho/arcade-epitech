/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** GameSnake
*/

#include "../../include/GameSnake.hpp"
#include "../../include/core.hpp"
#include <future>
#include <chrono>
#include <cstdlib>
#include <ctime>

game::GameSnake::GameSnake()
{
}

std::vector<std::string> game::GameSnake::replaceApple(std::vector<std::string> Map)
{
    int varX;
    int varY;
    do {
        varX = (rand()%Map.size() + 1);
        varY = (rand()%Map[0].size() + 1);
    } while (Map[varX][varY] != '*');
    Map[varX][varY] = 'P';
    this->score += 1;
    return (Map);
}
std::vector<std::string> game::GameSnake::findBody(std::vector<std::string> Map)
{
    if (Map[posX_tail][posY_tail - 1] == 'o')
        Map[posX_tail][posY_tail - 1] = 'q';
    else if (Map[posX_tail][posY_tail + 1] == 'o')
        Map[posX_tail][posY_tail + 1] = 'q';
    else if (Map[posX_tail - 1][posY_tail] == 'o')
        Map[posX_tail - 1][posY_tail] = 'q';
    else if (Map[posX_tail + 1][posY_tail] == 'o')
        Map[posX_tail + 1][posY_tail] = 'q';
    Map[posX_tail][posY_tail] = '*';
    return(Map);
}

void game::GameSnake::read_highscore()
{
  std::ifstream file("highscore_snake.txt", std::ifstream::in);
  std::string buffer;
  std::string str_score;
  std::getline(file, buffer);
  int pos = buffer.find_last_of(' ');
  file.close();
  str_score = buffer.substr(pos);
  this->highscore = atoi(str_score.c_str());
}

void game::GameSnake::write_highscore()
{
    if (this->score > this->highscore) {
        this->highscore = this->score;
        std::ofstream file("highscore_snake.txt", std::ofstream::out);
        std::string buffer;
        buffer = "PSEUDO " + std::to_string(this->highscore);
        std::cout << buffer << std::endl;
        file << buffer;
        file.close();
    }
}

std::vector<std::string> game::GameSnake::beginSnake(std::vector<std::string> Map, int touche)
{
    this->corp = 0;
    this->touchePress = touche;
    for (size_t i = 0; i < Map.size(); i++) {
        for (size_t j = 0; j < Map[0].size(); j++) {
            if (Map[i][j] == 'O') {
                this->posX = i;
                this->posY = j;
            }
            if (Map[i][j] == 'q') {
                this->posX_tail = i;
                this->posY_tail = j;
            }
        }
    }
        if (touchePress == 1) {
            if (Map[this->posX][this->posY - 1] != '*' && Map[this->posX][this->posY - 1] != 'P') {
                this->Exit = 1;
                core c;
                c.game_over_snake(this->score);
                //exit(EXIT_SUCCESS);
            }
            if (Map[this->posX][this->posY - 1] == 'P')
                Map = replaceApple(Map);
            else
                Map = findBody(Map);
            Map[this->posX][this->posY - 1] = 'O';          
            Map[this->posX][this->posY] = 'o';          
        }
        else if (touchePress == 2) {
            if (Map[this->posX - 1][this->posY] != '*' && Map[this->posX - 1][this->posY] !='P') {
                this->Exit = 1;
                core c;
                c.game_over_snake(this->score);
                //exit(EXIT_SUCCESS);
            }
            if (Map[this->posX - 1][this->posY] == 'P')
                Map = replaceApple(Map);
            else
                Map = findBody(Map);
            Map[this->posX - 1][this->posY] = 'O';
            Map[this->posX][this->posY] = 'o';
        }
        else if (touchePress == 3) {
            if (Map[this->posX][this->posY + 1] != '*' && Map[this->posX][this->posY + 1] !='P') {
                this->Exit = 1;
                core c;
                c.game_over_snake(this->score);
                //exit(EXIT_SUCCESS);
            }
            if (Map[this->posX][this->posY + 1] == 'P')
                Map = replaceApple(Map);
            else
                Map = findBody(Map);
            Map[this->posX][this->posY + 1] = 'O';
            Map[this->posX][this->posY] = 'o';          
        }
        else if (touchePress == 4) {
            if (Map[this->posX + 1][this->posY] != '*' && Map[this->posX + 1][this->posY] !='P') {
                this->Exit = 1;
                core c;
                c.game_over_snake(this->score);
                //exit(EXIT_SUCCESS);
            }
            if (Map[this->posX + 1][this->posY] == 'P')
                Map = replaceApple(Map);
            else
                Map = findBody(Map);
            Map[this->posX + 1][this->posY] = 'O';
            Map[this->posX][this->posY] = 'o';          
        }
        if (this->score >= 2)
            this->Exit = 2;
    return (Map);
}


game::GameSnake::~GameSnake()
{
}
