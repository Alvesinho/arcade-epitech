/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** ncurses_menu
*/

#ifndef NCURSES_MENU_HPP_
#define NCURSES_MENU_HPP_

#include <ncurses.h>
#include <cstdlib>
#include <string>
#include <cstring>
#include <curses.h>

class menu_ncurses {
    public:
        menu_ncurses();
        ~menu_ncurses();
        int game;
        int lib;
        int ncurses_menu();
        int snake_menu();
        int pacman_menu();
    protected:
    private:
        WINDOW *menu_snake, *w, *menu_pacman, *rules_snake, *rules_pacman, *highscore_snake;
        
};

#endif
