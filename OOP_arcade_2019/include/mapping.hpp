/*
** EPITECH PROJECT, 2020
** inho
** File description:
** include mapping
*/

#ifndef _MAPPING_HPP_
#define _MAPPING_HPP_

#include "include.hpp"

namespace arcadeGame
{
  class SFMLGame : public arcade::SFMLGraphic 
  {
  public:
    SFMLGame(void);
    virtual ~SFMLGame(void);
    std::vector<std::string> putWall(std::vector<std::string> Map);
    virtual int GameSnake(std::vector<std::string> Map);
    virtual int IsKeyPressedGame(void);
    virtual int init(void);
    virtual int IsOpenGame(std::vector<std::string> Map);
    virtual int JeuContinue(std::vector<std::string> Map); 
    virtual void IsEventGame(std::vector<std::string> Map);
    virtual void IsCloseGame(void);
    virtual void drawwindowGame(void);
    virtual void drawdisplayGame(void);
    virtual void IsCloseWin(void);
    virtual void stockline(std::vector<std::string> Map);
    int touchegame;

  private:
    int oneTime = 0;
    int stock = 0;
    int lever = 0;
    int xmap = 1;
    int ymap = 1;
    sf::Sprite sprite;
    sf::Texture sprite_t;
    sf::RectangleShape board;
    sf::RectangleShape plato;

  protected:
    int Column;
    int line;
    sf::Font font2;
    sf::Sprite wall;
  	sf::Texture wall_t;
    sf::Sprite food;
  	sf::Texture food_t;
    sf::Sprite snake;
  	sf::Texture snake_t;
    sf::Sprite body;
  	sf::Texture body_t;
    sf::Sprite ghost1;
  	sf::Texture ghost1_t;
    sf::Sprite ghost2;
  	sf::Texture ghost2_t;
    sf::Sprite ghost3;
  	sf::Texture ghost3_t;
    sf::Sprite ghost4;
  	sf::Texture ghost4_t;
    sf::Sprite pacgomme;
  	sf::Texture pacgomme_t;
    sf::Sprite pacboule;
  	sf::Texture pacboule_t;

    sf::Text score;
  };
}

#endif

