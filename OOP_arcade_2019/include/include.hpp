#ifndef _SFMLGRAPHIC_HPP_
#define _SFMLGRAPHIC_HPP_

#include <SFML/Graphics.hpp>
#include <iostream>
#include <utility>

namespace arcade
{
  class SFMLGraphic
  {
  public:
    SFMLGraphic(void);
    virtual  ~SFMLGraphic(void);
    virtual int IsOpen(void);
    virtual int commetuveux(void);
    virtual void IsEvent(void);
    virtual void IsClose(void);
    virtual void IsMousePressed(void);
    virtual void IsKeyPressed(void);
    virtual void drawmenu(void);
    virtual void drawmenusnk(void);
   // virtual void drawgamesnk(void);
    virtual void drawwindow(void);
    virtual void setTexture(void);
    virtual void initBox(void);
    virtual void initText(void);
    virtual void initTextSnake(void);
    virtual void initTextPacman(void);
    virtual void ActionMenu(void);
    virtual void ActionSnake(void);
    virtual void ActionPacman(void);
    virtual void drawmenupac(void);
    virtual void drawrulessnk(void);
    virtual void drawrulespac(void);
    virtual void drawhs(void);

//    void putWall(std::vector<std::string> Map);
    void initMenu(void);

  protected:
      int stock = 0;
      int lever = 0;
      int jeu = 0;
      sf::Sprite sprite;
      sf::Texture texture;
      sf::Font font;
      sf::RenderWindow window;
      sf::Text systeme;
      sf::Text bt_hostname;
      sf::Text bt_systeme;
      sf::Text bt_time;
      sf::Text bt_RAM;
      sf::Text bt_HS;
      sf::Text bt_play;
      sf::Text bt_play2;
      sf::Text bt_rules;
      sf::Text bt_rules2;
      sf::Text bt_exit;
      sf::Text bt_exit2;
      sf::Text hightscore;
      sf::Event event;
      sf::RectangleShape ids1;
      sf::RectangleShape ids2;
      sf::RectangleShape ids3;
      sf::RectangleShape ids4;
      sf::RectangleShape ids5;
      sf::Sprite snkmenu;
      sf::Texture snkmenu_t;
      sf::Sprite pac;
      sf::Texture pacmenu_t;
      sf::Sprite snkrules;
      sf::Texture snkrules_t;
      sf::Sprite pacrules;
      sf::Texture pacrules_t;
      sf::RectangleShape snk_play;
      sf::RectangleShape snk_exit;
      sf::RectangleShape snk_rules;
      sf::RectangleShape snk_play2;
      sf::RectangleShape snk_exit2;
      sf::RectangleShape snk_rules2;
//    sf::RectangleShape board;
//    sf::RectangleShape plato;

  };
}

#endif

