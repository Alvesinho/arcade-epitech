/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** arcade
*/

#ifndef ARCADE_HPP_
#define ARCADE_HPP_

#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <unistd.h>

class virtual_core {
    public:
        virtual_core();
        virtual void game_over_snake();
        virtual void game_win_snake();
        virtual void chooseLib(int);
        virtual void chooseGame(int);
        virtual int go_to_sdl();
        virtual int go_to_sfml();
        virtual int go_to_sdl_snake();
        virtual int go_to_ncurses();
        ~virtual_core(void);
    protected:
        std::vector<std::string> Map;
        std::vector<std::string> MapPacman;
        int touche;
        int checkLib;
        int checkGame;
    private:
};

#endif /* !ARCADE_HPP_ */
