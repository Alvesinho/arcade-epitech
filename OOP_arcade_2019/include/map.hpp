/*
** EPITECH PROJECT, 2020
** Map
** File description:
** Inho
*/

#ifndef _MAP_HPP_
#define _MAP_HPP_

#include "include.hpp"

namespace arcadeGamePac
{
  class SFMLGamePac : public arcade::SFMLGraphic 
  {
  public:
    SFMLGamePac(void);
    virtual ~SFMLGamePac(void);
    std::vector<std::string> putWallPac(std::vector<std::string> Map);
    virtual int GameSnakePac(std::vector<std::string> Map);
    virtual int IsKeyPressedGamePac(void);
    virtual int initPac(void);
    virtual int IsOpenGamePac(std::vector<std::string> Map);
    virtual int JeuContinuePac(std::vector<std::string> Map);
    virtual void IsEventGamePac(std::vector<std::string> Map);
    virtual void IsCloseGamePac(void);
    virtual void IsCloseWinPac(void);
    virtual void drawwindowGamePac(void);
    virtual void drawdisplayGamePac(void);
    virtual void stocklinePac(std::vector<std::string> Map);
    int touchegamePac;

  private:
    int oneTime = 0;
    int stock = 0;
    int lever = 0;
    sf::Sprite sprite;
    sf::Texture sprite_t;
    sf::RectangleShape board;
    sf::RectangleShape plato;

  protected:
    int Column;
    int line;
    sf::Font font2;
    sf::Sprite wall;
  	sf::Texture wall_t;
    sf::Sprite food;
  	sf::Texture food_t;
    sf::Sprite snake;
  	sf::Texture snake_t;
    sf::Sprite body;
  	sf::Texture body_t;
    sf::Sprite ghost1;
  	sf::Texture ghost1_t;
    sf::Sprite ghost2;
  	sf::Texture ghost2_t;
    sf::Sprite ghost3;
  	sf::Texture ghost3_t;
    sf::Sprite ghost4;
  	sf::Texture ghost4_t;
    sf::Sprite pacgomme;
  	sf::Texture pacgomme_t;
    sf::Sprite pacboule;
  	sf::Texture pacboule_t;
    sf::Sprite win;
  	sf::Texture win_t;
    sf::Text score;
  };
}

#endif

