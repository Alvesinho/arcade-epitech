/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** SDL_Snake
*/

#ifndef SDL_SNAKE_HPP_
#define SDL_SNAKE_HPP_
#include "SDL.hpp"
#include <iostream>
#include <string>


class SDL_Snake : public SDL
{
    public:
        SDL_Snake();
        ~SDL_Snake();
        void init_image_play();
        void freeSpace_play();
        void pause_play();
        void flip_ecran_play();
        void blit_to_surface_play();
        void init_position_play();
        void init_background_play();
        void press_up();
        void press_down();
        void press_right();
        void press_left();
        void check_end();
        void print_snake();
        int i;
        uint8_t *touche;
        SDL_Surface *snake;
        SDL_Surface *carreSnake;
        SDL_Surface *carreSnake2;
        SDL_Surface *carreSnake3;
        SDL_Rect positionSnake;
        SDL_Rect positionCarreSnake;
        SDL_Rect positionCarreSnake2;
        SDL_Rect positionCarreSnake3;
        SDL_Surface *snakeTab[500];

    protected:
    private:
};

#endif /* !SDL_SNAKE_HPP_ */
