/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** Parse
*/

#ifndef PARSE_HPP_
#define PARSE_HPP_
#include "core.hpp"

namespace Arcade
{
    class Parse : public core
    {
        public:
        Parse();
        std::vector<std::string> readMap();
        std::vector<std::string> readMapPacman();
        void checkCorp(std::vector<std::string>, int x, int y);
        ~Parse();
        private:
        static std::vector<int>tabCordX;
        static std::vector<int>tabCordY;
        protected:
        int addTabCord;
        int nbCorp = 0;
        int line;
        int colone;
        private:
    };
}

#endif /* !PARSE_HPP_ */
