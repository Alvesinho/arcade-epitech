/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** SDL_Map
*/

#ifndef SDL_MAP_HPP_
#define SDL_MAP_HPP_
#include "core.hpp"
#include "SDL.hpp"

namespace SDL_map
{
    class SDL_Map : public SDL
    {
        public:
            SDL_Map();
            int SDL_drawMap(std::vector<std::string> Map);
            void init_background_map();
            void init_image_map();
            void init_position_map();;
            void flip_ecran_map();
            void loadSprite();
            void blit_to_surface_map();
            std::vector<std::string> putWall(std::vector<std::string> Map);
            void freeSpaceMap();
            void stock_line_and_column(std::vector<std::string> Map);
            void pauseMap();
            void SDL_Win();
            void SDL_gameOver();
            ~SDL_Map();
        protected:
        int touche;
        int Column;
        int line;
        SDL_Surface *imageMur;
        SDL_Surface *imageSnakehead;
        SDL_Surface *imageSnakeCorp;
        SDL_Surface *imagePomme;
        SDL_Surface *imagePacGomme;
        SDL_Surface *imagePacMan;
        SDL_Surface *imageGhost1;
        SDL_Surface *imageGhost2;
        SDL_Surface *imageGhost3;
        SDL_Surface *imageGhost4;
        SDL_Surface *imageGhost5;
        SDL_Surface *imagePacBoule;
        SDL_Rect positionPacBoule;
        SDL_Rect positionGhost1;
        SDL_Rect positionGhost2;
        SDL_Rect positionGhost3;
        SDL_Rect positionGhost4;
        SDL_Rect positionGhost5;
        SDL_Rect positionPacMan;
        SDL_Rect positionPacGomme;
        SDL_Rect positionPomme;
        SDL_Rect positionSnakeCorp;
        SDL_Rect positionSnakeHead;
        SDL_Rect positionMur;
        private:
    };
    
}

#endif /* !SDL_MAP_HPP_ */