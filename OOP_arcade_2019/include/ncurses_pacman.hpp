/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** ncurses_pacman
*/

#ifndef NCURSES_PACMAN_HPP_
#define NCURSES_PACMAN_HPP_

#include <ncurses.h>
#include <cstdlib>
#include <string>
#include <cstring>
#include <curses.h>
#include <vector>
#include "ncurses_menu.hpp"
#include "core.hpp"

class pacman_ncurses 
{
    public:
        int map_ncurses_pacman(std::vector<std::string> Map);
        int game_loop_pacman(std::vector<std::string> Map);
        int lib;
        int game;
    protected:
        
    private:
        WINDOW *pacman_win;
};

#endif /* !NCURSES_PACMAN_HPP_ */
