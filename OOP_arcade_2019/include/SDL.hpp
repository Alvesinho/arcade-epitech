/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** SDL
*/

#ifndef SDL_HPP_
#define SDL_HPP_
#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <stdbool.h>


class SDL {
    public:
        SDL();
        ~SDL();
        void init_background();
        void init_position();
        void blit_to_surface();
        void init_image();
        void flip_ecran();
        int pause();
        void freeSpace();
        int changeToMouse();
        int goToMenuSnak();
        int goToMenuPacMan();
    int stockNb;
    SDL_Event event;
    SDL_Surface *ecran;
    SDL_Surface *imageFond;
    SDL_Surface *snackBlanc;
    SDL_Surface *sfmlBlanc;
    SDL_Surface *ExitBlanc;
    SDL_Surface *HsBlanc;
    SDL_Surface *NcurseBlanc;
    SDL_Surface *PacmanBlanc;
    SDL_Surface *PseudoBlanc;
    SDL_Surface *SdlBlanc;
    SDL_Rect positionSdlBlanc;
    SDL_Rect positionPseudoBlanc;
    SDL_Rect positionPacmanBlanc;
    SDL_Rect positionNcurseBlanc;
    SDL_Rect positionHsBlanc;
    SDL_Rect positionExitBlanc;
    SDL_Rect positionSfmlBlanc;
    SDL_Rect positionSnackBlanc;
    SDL_Rect positionFond;
    SDL_Rect positionSouris;
    protected:
    private:
};

#endif /* !SDL_HPP_ */
