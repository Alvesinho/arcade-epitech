/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** SDL_MenuPacMan
*/

#ifndef SDL_MENUPACMAN_HPP_
#define SDL_MENUPACMAN_HPP_
#include "SDL.hpp"

class SDL_MenuPacMan : public SDL
{
    public:
        SDL_MenuPacMan();
        ~SDL_MenuPacMan();
        void init_image_Pacman();
        void freeSpace_PacMan();
        int pause_PacMan();
        void flip_ecran_PacMan();
        void blit_to_surface_PacMan();
        void init_position_snake();
        int changeToMousePacMan();
        void goToPlay();
        void init_background_snake();

        SDL_Surface *background_menuPacMan;
        SDL_Surface *playBlancP;
        SDL_Surface *rulesBlancP;
        SDL_Surface *mainMenuP;
        SDL_Surface *exitP;
        SDL_Surface *Rule;



    protected:
    private:
};

#endif /* !SDL_MENUPACMAN_HPP_ */
