/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** SDL_MenuSnak
*/

#ifndef SDL_MENUSNAK_HPP_
#define SDL_MENUSNAK_HPP_
#include "SDL.hpp"

class SDL_MenuSnak : public SDL
{
    public:
        SDL_MenuSnak();
        ~SDL_MenuSnak();
        void init_image_snake();
        void freeSpace_snake();
        int pause_snake();
        void flip_ecran_snake();
        void blit_to_surface_snake();
        void init_position_snake();
        int changeToMouseSnake();
        void goToPlay();
        void init_background_snake();

        SDL_Surface *background_menuSnake;
        SDL_Surface *playBlanc;
        SDL_Surface *rulesBlanc;
        SDL_Surface *mainMenu;
        SDL_Surface *Rules;
        int Exit = 0;



    protected:
    private:
};

#endif /* !SDL_MENUSNAK_HPP_ */
