/*
** EPITECH PROJECT, 2020
** OOP_arcade_2019
** File description:
** pacman
*/

#ifndef PACMAN_HPP_
#define PACMAN_HPP_
#include "core.hpp"
#include "Parse.hpp"


namespace Pac
{
    class pacman : public Arcade::Parse
    {
        private:
        public:
            pacman(void);
            void read_highscore_pacman();
            void write_highscore_pacman();
            double score = 0.0;
            double highscore;
            std::vector<std::string> beginPacman(std::vector<std::string>, int);
            ~pacman(void);
        protected:
            int touchePressed;
            int posX;
            int posY;
           
    };
}

#endif /* !PACMAN_HPP_ */
